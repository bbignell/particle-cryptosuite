#ifndef ParticleCryptosuite_h
#define ParticleCryptosuite_h

#include "Particle.h"

#include "Sha1.h"
#include "Sha256.h"

class ParticleCryptosuite {
  public:
    String SHA1(const String source, const bool upper = true);
    String HmacSHA1(const byte* const key, const String source, const bool upper = true);
    String SHA256(const String source, const bool upper = true);
    String HmacSHA256(const byte* const key, const String source, const bool upper = true);
};

extern ParticleCryptosuite Digest;

#endif
