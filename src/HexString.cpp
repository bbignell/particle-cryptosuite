#include "HexString.h"

const char* const HexString::lowerHexmap = "0123456789abcdef";
const char* const HexString::upperHexmap = "0123456789ABCDEF";

// Converts an array of bytes to a hex string, using either uppercase (default)
// or lowercase letters. The bytes array must not be null.
String HexString::toString(const uint8_t* const bytes, const bool upper) {
  String result = String();
  const char* charMap = upper ? upperHexmap : lowerHexmap;
  for (int i = 0; i < sizeof(bytes); ++i) {
    result.concat(charMap[bytes[i]>>4]);
    result.concat(charMap[bytes[i]&0xf]);
  }
  return result;
}
