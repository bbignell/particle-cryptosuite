#include "ParticleCryptosuite.h"

String ParticleCryptosuite::SHA1(const String source, const bool upper) {
  Sha1.init();
  Sha1.print(source);
  return Sha1.toString(Sha1.result(), upper);
}

String ParticleCryptosuite::HmacSHA1(const byte* const key, const String source, const bool upper) {
  Sha1.initHmac(key, sizeof(key));
  Sha1.print(source);
  return Sha1.toString(Sha1.resultHmac(), upper);
}

String ParticleCryptosuite::SHA256(const String source, const bool upper) {
  Sha256.init();
  Sha256.print(source);
  return Sha256.toString(Sha256.result(), upper);
}

String ParticleCryptosuite::HmacSHA256(const byte* const key, const String source, const bool upper) {
  Sha256.initHmac(key, sizeof(key));
  Sha256.print(source);
  return Sha256.toString(Sha256.resultHmac(), upper);
}

ParticleCryptosuite Digest;
