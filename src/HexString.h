#ifndef HexString_h
#define HexString_h

#include "Particle.h"

class HexString {
  public:
    String toString(const uint8_t* const bytes, const bool upper = true);

  private:
    static const char* const lowerHexmap;
    static const char* const upperHexmap;
};

#endif
