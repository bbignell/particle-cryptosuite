# Introduction

ParticleCryptosuite is a set of hashing tools for Particle devices, based on
the work done in https://github.com/Cathedrow/Cryptosuite for Arduino.

The following hashing algorithms are provided:

* SHA-1
* HMAC-SHA-1
* SHA-256
* HMAC-SHA-256

# Examples

The /examples folder contains detailed examples for hashing with SHA-1 and
SHA-256, including HMAC. The Digest class provides simple,
one-line hashing or you can use the Sha1 and Sha256 classes for more
flexibility.

## SHA-1

Using Digest, you can create hashes with uppercase or lowercase letters,
depending on your needs:

    #include "ParticleCryptosuite.h"

    String hash = Digest.SHA1("this is my message"); // ex: ABCD0123
    String lowerHash = Digest.SHA1("this is my message", false); // ex: abcd0123


Using Sha1, you can achieve the same result:

    #include "ParticleCryptosuite.h" // or "sha1.h"

    Sha1.init();
    Sha1.print("this is my message");
    uint8_t* hashBytes = Sha1.result();
    String hash = Sha1.toString(hashBytes);



## HMAC-SHA-1

Using Digest:

    #include "ParticleCryptosuite.h"

    String hash = Digest.HmacSHA1("this is my key", "this is my message");
    String lowerHash = Digest.HmacSHA1("this is my key", "this is my message");

Using Sha1:

    #include "ParticleCryptosuite.h" // or "sha1.h"

    char* key = "this is my message";
    Sha1.initHmac(key, sizeof(key));
    Sha1.print("this is my message");
    uint8_t* hashBytes = Sha1.resultHmac();
    String hash = Sha1.toString(hashBytes);


## SHA-256 and HMAC-SHA-256

Hashing with SHA-256 and HMAC-SHA-256 is done as above, using either the
methods provided by the Digest class, or with the Sha256 class directly.


## Verification
The provided example code tests against published test vectors.

* SHA-1: FIPS 180-2, RFC3174 compliant
* HMAC-SHA-1: FIPS 198a compliant
* SHA-256: FIPS 180-2, RFC4231 compliant
* HMAC-SHA-256:  RFC4231 compliant
